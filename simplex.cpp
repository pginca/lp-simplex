#include<bits/stdc++.h>
#include "simplex.h"

/*
  is_feasible_basis returns 1 if a basis is feasible and
  0 otherwise. A base being feasible means that all
  values in the first column of A are greater than zero.
*/
int is_feasible_basis(int n, double A[][MAX], int base[]){

 int i;

  FOR(i, 1, n){
    if(base[i] != 0){
      if(A[base[i]][0] < 0)
          return 0;
    }
  }
  return 1;
}

/*
  has_entry_value returns the column of the variable
  that is going to enter the base or -1 if there is
  no available candidates. If there is no candidate,
  then we have reached the maximum.
*/
int has_entry_value(int m, int n, double A[][MAX]){

  int i, k = -1;
  double min = DBL_MAX;

  FOR(i, 1, n){
    if((A[0][i] < 0) and (A[0][i] < min)){
      min = A[0][i];
      k = i;
    }
  }

  if(k == -1)
  	return -1;
  else
    return k;
}


/*
  has_exit_value returns the row of the variable that
  is going to leave the basis, or -1 if there is no
  candidate available. If there is no candidate,
  then the LP is UNBOUNDED.
*/
int has_exit_value(int m, double A[][MAX], int pivot_col){

  int i, k = 0;
  double min = DBL_MAX;

  FOR(i, 1, m)
    /*
      The candidate has to be greater than zero 
      and have the minimum growth limit.
    */
    if((A[i][pivot_col] > 0) && (A[i][0] < min)){
      min = A[i][0];
      k = i;
    }

  if(k == 0)
    return -1;


  return k;
}

/*
  pivots A in the column pivot_col and row pivot_row
*/
void pivot(int m, int n, double A[][MAX], int pivot_col, int pivot_row){

  int i, j;
  double m_pivot, A_pivot;

  A_pivot = A[pivot_row][pivot_col];

  FOR(i, 0, n)
    A[pivot_row][i] = (A[pivot_row][i] / A_pivot);

  FOR(i, 0, m){

    if(i != pivot_row){
      m_pivot = -A[i][pivot_col] / A[pivot_row][pivot_col];

      FOR(j, 0, n){
       
        A[i][j] = (A[pivot_row][j] * m_pivot) + A[i][j];

      }
    }
  }
}

/*
  update_base removes from base the exiting value and
  inserts the entering value
*/
void update_base(int m, int n, int pivot_col, int pivot_row, int base[]){

  int i;

  // Exiting value
  FOR(i, 0, n+m)
    if(base[i] == pivot_row)
      base[i] = 0;

  // Entering value
  base[pivot_col] = pivot_row;

}


/*
  solver receives a tableau A, finds the a variable
  to enter a variable to leave the base and pivot
  the tableau 
*/
int solver(int m, int n, double A[][MAX], int base[]){

  int pivot_row, pivot_col, i, j; 

  pivot_col = has_entry_value(m, n, A);

  /* if pivot_col == -1 the solver solved the LP */
  if(pivot_col == -1){
    return FEA;
  }
	else {
    pivot_row = has_exit_value(m, A, pivot_col);

    /* if pivot_row == -1 there is no variable to 
       leave the base and the LP is unbounded */
    if(pivot_row == -1){
      return UNBD;
    }
    else{

      pivot(m, n, A, pivot_col, pivot_row);
      
      update_base(m, n, pivot_col, pivot_row, base);

      solver(m, n, A, base);
    }
  }

}


/*
  add auxiliary variables when the initial basic solution
  is not feasible
*/
void add_auxiliary_variables(int m, int n, double A[][MAX]){

  int i, j, k;

  FOR(i, 0, m) 
    FOR(j, n+1, n+m)
      A[i][j] = 0;

  k = m - 1;
  FOR(i, 1, m){
    if(A[i][0] < 0)
      A[i][n + m - k] = -1;
    else
      A[i][n + m - k] = 1;
    k--;
  }

  FOR(i, 0, m){

    if(A[i][0] < 0){
 
      FOR(j, 0, n+m)
        A[i][j] = -A[i][j];
    }
  }
}

/*
  add the new objective function for the auxiliary LP
*/
void add_auxiliary_objective_function(int m, int n, int base[], double A[][MAX]){

  int i, k, u, j;

  FOR(i, n+1, n+m)
    A[0][i] = 1;

  FOR(i, 1, n)
    A[0][i] = 0;

  k = m - 1;
  FOR(i, 1, m){
    pivot(m, n, A, n + m - k, i);
    //update_base(m, n, n + m - k, i, base);
    k--;
  }

}

/* create new base for the auxiliary LP */
void new_base(int m, int n, int base[]){

  int i, j;

  FOR(i, 1, n) 
    base[i] = 0;

  i = 0;
  j = m + n;
  while(m - i > 0){
    base[j] = m - i;
    i++;
    j--;
  }

}

int simplex(int m, int n, double z[], double A[][MAX], double b[],
            double *z0, double x[]){

  int i, j, feasible, ans, k, acc = 0;
  int base[MAX];

  /* inserts z in row 0 of A */
  FOR(i, 0, n) A[0][i] = -z[i];

  /* inserts b in col 0 if A */
  FOR(i, 0, m) A[i][0] = b[i];

  /* build up the first base */
  i = 0;
  j = n;
  while(m - i > 0){
    base[j] = m - i;
    i++;
    j--;
  }

  FOR(i, 1, n)
    if(A[0][i] != 0)
      acc++;

  /* check if the first base is feasible */
  if((is_feasible_basis(n, A, base)) && (acc < n - m)){

    ans = solver(m, n, A, base);
  }
  else{

    /* build up the auxiliary LP */
    add_auxiliary_variables(m, n, A);
    new_base(m, n, base);
    add_auxiliary_objective_function(m, n, base, A);

    /* solve the auxiliary LP */
    solver(m, n, A, base);

    /* if a feasible initial solution was found */
    if(fabs(A[0][0]) < EPS){

      /* if there is auxiliary variables in the base 
         there need to become nonbasic. It will be
         a degenerate pivot step */
      int changed = 0;
      FOR(i, m+1, m+n)
        if(base[i] != 0){
          FOR(j, 1, n){
            if(A[0][j] != 0){
              pivot(m, n+m, A, j, base[i]);
              update_base(m, n+m, j, base[i], base);
              j = m+n;
              changed = 1;
            }
          }
          if(changed == 0){
            FOR(j, 0, n+m)
              if(j != i)
                A[base[i]][j] = 0.0;
            update_base(m, n+m, base[i], base[i], base);
          }
        }

      /* gets back the original objective function */
      FOR(i, 0, n) A[0][i] = -z[i];

      /* pivot with the original objective function 
         and the initial feasible solution */
      FOR(i, 0, m)
        if(A[0][i] < 0)
          pivot(m, n, A, i, base[i]);

      /* solver theLP with the initial feasible solution found */
      ans = solver(m, n, A, base);

      
    }
    else return NFEA;
  }

  /* writes the answer found in the output array */
  if(ans == FEA){
    FOR(i, 1, m)
      x[i] = A[base[i]][0];

    *z0 = A[0][0];
  }

  return ans;
}

