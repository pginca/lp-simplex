#ifndef _SIMPLEX_
#define _SIMPLEX_

#define MAX 1123 /* ~10^3      */
#define FEA   0  /* FEAsible   */
#define NFEA -1  /* iNFEAsible */
#define UNBD  1  /* UNBounDed  */

#define EPS 1e-9 /* 10^-9 */
#define correct_double(var) \
  (fabs(var) < EPS ? 0. : var) /* Avoid printing -0.000 */
#define FOR(var, a, b) \
  for (var = (a); var <= (b); ++var) /* Avoid silly mistakes ;) */


int simplex(int m, int n, double z[], double A[][MAX], double b[],
            double *z0, double x[]);

#endif
